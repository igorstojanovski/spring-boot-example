package org.igorski.example.services;

import org.igorski.example.model.Team;
import org.igorski.example.repositories.TeamRepository;
import org.igorski.example.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TeamServiceTest {

    @Mock
    private TeamRepository teamRepository;
    @Mock
    private UserRepository userRepository;

    @Test
    public void shouldTestSomething() {
        TeamService teamService = new TeamService(teamRepository, userRepository);

        Team team = new Team();
        team.setId(1L);
        team.setName("BestOne");

        teamService.createTeam(team);
        verify(teamRepository).save(team);
    }


}
